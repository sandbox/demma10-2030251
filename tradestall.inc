<?php

/**
 * @file
 * Defines the tradestall component type.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_tradestall() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'width' => '',
      'maxlength' => '',
      'field_prefix' => '',
      'field_suffix' => '',
      'disabled' => 0,
      'unique' => 0,
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
      'mapfile' => '',
      'maxstalls' => '',
      'maxvalues' => 1,
      'is_shared' => 'no',
    ),
  );
}


/**
 * Implements _webform_edit_component().
 */
function _webform_edit_tradestall($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.') . theme('webform_token_help'),
    '#size' => 60,
    '#maxlength' => 1024,
    '#weight' => 0,
  );
  $form['display']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $component['extra']['width'],
    '#description' => t('Width of the textfield.') . ' ' . t('Leaving blank will use the default size.'),
    '#size' => 5,
    '#maxlength' => 10,
    '#weight' => 0,
    '#parents' => array('extra', 'width'),
  );
  $form['display']['field_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix text placed to the left of the textfield'),
    '#default_value' => $component['extra']['field_prefix'],
    '#description' => t('Examples: $, #, -.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.1,
    '#parents' => array('extra', 'field_prefix'),
  );
  $form['display']['field_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Postfix text placed to the right of the textfield'),
    '#default_value' => $component['extra']['field_suffix'],
    '#description' => t('Examples: lb, kg, %.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.2,
    '#parents' => array('extra', 'field_suffix'),
  );
  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );
  $form['validation']['unique'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique'),
    '#return_value' => 1,
    '#description' => t('Check that all entered values for this field are unique. The same value is not allowed to be used twice.'),
    '#weight' => 1,
    '#default_value' => $component['extra']['unique'],
    '#parents' => array('extra', 'unique'),
  );
  $form['validation']['maxlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Maxlength'),
    '#default_value' => $component['extra']['maxlength'],
    '#description' => t('Maximum length of the textfield value.'),
    '#size' => 5,
    '#maxlength' => 10,
    '#weight' => 2,
    '#parents' => array('extra', 'maxlength'),
  );

  $form['extra']['mapfile'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to CSS Map File'),
    '#default_value' => $component['extra']['mapfile'],
    '#description' => t('Enter the full Drupal path to the CSS file containing the venue map. This will be used to position the available stalls in the selection picture. This CSS file and any accompanying pictures will need to be uploaded to the server separately.'),
    '#size' => 50,
    '#weight' => 2,
    '#parents' => array('extra', 'mapfile'),
  );

  $form['extra']['maxstalls'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of available stalls'),
    '#default_value' => $component['extra']['maxstalls'],
    '#description' => t('Enter the maximum number of available stalls.'),
    '#size' => 50,
    '#weight' => 2,
    '#parents' => array('extra', 'maxstalls'),
  );

  //The tradestall component needs to be multivalue to allow visitors to purchase more than
  //one tradestall on the one form. The setting maxvalues sets the maximum number of
  //values which may be entered into this field.
  //Because the textfield component is not multivalue we need to have a way of simulating
  //multivalue behaviour. This will need to be done in custom validation handlers.
  //Values are entered into the field as a comma-separated list.
  $form['extra']['maxvalues'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of Values'),
    '#default_value' => $component['extra']['maxvalues'],
    '#description' => t('Enter the maximum number of values which may be entered into this field. When entering multiple values separate each value by a comma.'),
    '#size' => 50,
    '#weight' => 2,
    '#parents' => array('extra', 'maxvalues'),
  );

  //Option to set whether data for this component is shared across
  //all webforms with the same mapfile. Note that components are still
  //independent of each other, setting shared on one component will not
  //affect the settings on other components.
  $form['extra']['is_shared'] = array(
    '#type' => 'select',
    '#title' => t('Is this a shared tradestall component?'),
    '#default_value' => empty($component['extra']['is_shared']) ? 'no' : $component['extra']['is_shared'],
    '#description' => t('This option sets whether submission data for this component is shared across all webforms with the same mapfile. If "yes" is selected then the submission data from other webforms will be included when determining which stalls are avaialable and which stalls are sold. If "no" is selected then the available and sold stalls will only be calculated from submissions to this webform.'),
    '#options' => array('no' => t('No'), 'yes' => t('Yes')),
    '#parents' => array('extra', 'is_shared'),
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 * The output of this function will be displayed on the form pages.
 */
function _webform_render_tradestall($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'webform_tradestall',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => $filter ? webform_replace_tokens($component['value'], $node) : $component['value'],
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? webform_filter_xss($component['extra']['field_prefix']) : $component['extra']['field_prefix']),
    '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? webform_filter_xss($component['extra']['field_suffix']) : $component['extra']['field_suffix']),
    '#description' => $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#attributes' => $component['extra']['attributes'],
    '#theme_wrappers' => array('webform_element'),
    '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
    '#mapfile' => $component['extra']['mapfile'],
    '#maxstalls' => $component['extra']['maxstalls'],
    '#maxvalues' => $component['extra']['maxvalues'],
    '#is_shared' => $component['extra']['is_shared'],
    '#nodeid' => $component['nid'],
    '#compid' => $component['cid'],
  );

  if ($component['extra']['disabled']) {
    if ($filter) {
      $element['#attributes']['readonly'] = 'readonly';
    }
    else {
      $element['#disabled'] = TRUE;
    }
  }

  // Enforce uniqueness.
  if ($component['extra']['unique']) {
    $element['#element_validate'][] = 'webform_validate_unique';
  }

  //Enforce the maximum number of selectable stalls.
  if($component['extra']['maxvalues'] == '') {
    $element['#element_validate'][] = 'tradestall_validate_maxvalues';
  }

  //Add a validator for the soldstalls. This validator is independent
  //of settings.
  $element['#element_validate'][] = 'tradestall_validate_soldstalls';

  // Change the 'width' option to the correct 'size' option.
  if ($component['extra']['width'] > 0) {
    $element['#size'] = $component['extra']['width'];
  }
  if ($component['extra']['maxlength'] > 0) {
    $element['#maxlength'] = $component['extra']['maxlength'];
  }

  if (isset($value)) {
    $element['#default_value'] = $value[0];
  }

  return $element;
}

/**
 * Implements _webform_display_component().
 * The output of this function is displayed on results pages.
 */
function _webform_display_tradestall($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_tradestall',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#field_prefix' => $component['extra']['field_prefix'],
    '#field_suffix' => $component['extra']['field_suffix'],
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

/**
 * Format the form component. This function simply uses a textfiled and adds the map
 * after the textfield.
 */
function theme_webform_tradestall($variables) {
  $element = $variables['element'];

  $mapdata = array('cid' => $element['#compid'], 'nid' => $element['#nodeid'], 'maxstalls' => $element['#maxstalls'], 'mapfile' => $element['#mapfile'], 'is_shared' => $element['#is_shared']);

  $venue_map = tradestall_create_venue_map($mapdata, $element['#value']);

  //Create the textfield by calling the texfield theme function directly.
  $textfield = theme_textfield($variables);

  //Create a hidden field to hold the maxvalues data. This will be used as
  //as a reference read-only field by the javascript code.
  $maxvals = check_plain($element['#maxvalues']);
  $hidden = '<input name="mapfile" type="hidden" readonly="readonly" value=' . $maxvals . " />\n";

  //Add the CSS for the venue map. If none exists then don't add any CSS.
  if($element['#mapfile'] != '') {
    $stylesheet = check_plain($element['#mapfile']);
    $csspath = DRUPAL_ROOT . $stylesheet;
    drupal_add_css($stylesheet);
    //Add the javascript code for interacting with the venue map.
    drupal_add_js(array('tradestall' => array('maxvalues' => $maxvals)), 'setting');
    $scriptfile = drupal_get_path('module', 'tradestall') . '/tradestall.js';
    drupal_add_js($scriptfile);
  }

  //Spiel text. These variables contain field instructions to be used for every
  //instance of the field.
  $map_insts = '<p>' . t('Click on a square to make your selection.') . '</p>';

  $noscript = '<noscript>' . t('You appear to have Javascript disabled. Please enter your selection into the textbox provided below.') . '</noscript>';

  $textfield_label = '<label>' . t('Stalls selected:') . '</label>';

  //Wrap all of this in a div to allow easier selection via javascript.
  $output = '<div class="tradestall">' . $map_insts . "\n" . $noscript . "\n" . $venue_map . "\n" . $hidden . "\n" . '<div class="tradestall_textbox">' . $textfield_label . "\n" . $textfield . '</div></div>';

  return $output;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_tradestall($variables) {
  $element = $variables['element'];
  $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  $suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];
  $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
  return $value !== '' ? ($prefix . $value . $suffix) : ' ';
}

/**
 * Implements _webform_analysis_component().
 * Not sure how to handle this function. Maybe I can report on how many tradestalls have
 * been sold. For the moment just leave the code from textfield here.
 */
function _webform_analysis_tradestall($component, $sids = array()) {
  //Prepare the nid and cid arrays.
  $nids = array();
  $cids = array();
  //Fill the nid and cid arrays.
  if($component['extra']['is_shared'] == 'yes') {
    //If this component is sharing submssion data retrieve
    //the nid and cid of all tradestall components with the
    //same mapfile and add them to the nid and cid arrays.

    //Grab all tradestall components. Unfortunately the extra
    //data is serialised so I can't query that directly. Hence
    //my need to extract the nid cid and extra data from ALL
    //tradestall components. If there is a way to do the mapfile
    //comparison in SQL then this code could be optimised.
    $query = db_select('webform_component', 'wc', array('fetch' => PDO::FETCH_ASSOC))
      ->fields('wc', array('nid', 'cid', 'extra'))
      ->condition('type', 'tradestall');
    $res = $query->execute();
    foreach ($res as $data) {
      //Now search through all returned data for components
      //with the same mapfile. If any are found then their
      //nid and cid values are added to teh nid and cid
      //array.
      $extradata = unserialize($data['extra']);
      if($extradata['mapfile'] == $component['extra']['mapfile']) {
        $nids[] = $data['nid'];
        $cids[] = $data['cid'];
      }
    }
  }
  else {
    //If this component is not sharing submission data just
    //add the current nid and cid to the arrays.
    $nids[] = $component['nid'];
    $cids[] = $component['cid'];
  }
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $nids, "IN")
    ->condition('cid', $cids, "IN");

  /*if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }*/

  $numstalls_sold = 0;
  $maxstalls = $component['extra']['maxstalls'];
  $percentage = 0;

  $result = $query->execute();
  //Get all of the stalls sold. We use the string concatenation trick
  //from tradestall_create_venue_map. Looks like I need to convert the
  //string into an array to get the count of how many elements are in
  //the string.
  $soldstalls = '';
  foreach ($result as $data) {
    //If data has been entered add it to the soldstalls string.
    //We use a length check to determine if data has been entered.
    if (drupal_strlen(trim($data['data'])) > 0) {
      $soldstalls .= ',' . check_plain(trim($data['data']));
    }
  }

  //Now to get the number of stalls sold. First convert the string
  //to an array, then filter out the empty values. Finally count the
  //values which are left. All this just to count the number of elements
  //in a string.
  $numstalls_sold = count(array_filter(explode(',', $soldstalls)));

  $rows[0] = array(t('Number of stalls sold'), $numstalls_sold);
  $rows[1] = array(t('Maximum number of stalls'), $maxstalls);
  $rows[2] = array(t('Number of stalls remaining'), ($maxstalls - $numstalls_sold));
  $rows[3] = array(t('Percentage of stalls sold'), round(($numstalls_sold / $maxstalls) * 100) . t('%'));
  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_tradestall($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_tradestall($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_tradestall($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}
