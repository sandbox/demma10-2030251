(function ($) {

  //A string parsing function to strip the "stall_" prefix from
  //the argument given and return what's left.
  Drupal.tradestallRemovePrefix = function (str) {
    //Define the number of characters of the string we want.
    var len = str.length - 6;
    //We want to cut from after the "stall_" part to the end of the string.
    return str.substr(6,len);
  };

  //Function which removes the first argument from a string.
  //At present I do this by converting to an array, removing
  //the item and then converting back into a string. There must
  //be a faster way to do this.
  Drupal.tradestallRemoveItem = function(str, item) {
    var arr = new Array();
    arr = str.split(",");
    arr.splice(arr.indexOf(item), 1);
    return arr.toString();
  };

  //The main function for this module.
  Drupal.behaviors.tradestall = {
    attach: function (context, settings) {

      //Set a global count variable to keep track of how many stalls
      //the current user has selected. At the moment it is global
      //across all components. Somehow I need to set this just for
      //for specific components (maybe I need another hidden HTML
      //element).
      var currstalls = 0;

      //Mark all textfields with the visibility:hidden CSS property.
      //I want the element to be part of the form (ie include space
      //for it) but I don't want it to be seen.
      $('div.tradestall_textbox').css("visibility","hidden");

      //Add the CSS pointer cursor to the element.
      //I add it in the Javascript file because I
      //don't want the pointer to show up when Javascript
      //is disabled and an element cannot be clicked on.
      $('.tradestall li[class!="sold"]').css("cursor", "pointer");

      //This function changes the class of a 
      //clicked tradestall list item. If the item
      //does not have the 'selected' class then it is
      //added, if it already has the class it is removed.
      $('.tradestall li', context).click(function () {
        //Only execute if the clicked-on stall is not already
        //sold. Maybe I can build this into a selector.
        if(!$(this).hasClass("sold")) {
          //First get to the root div of this element.
          //I use the fact that this is a webform element
          //to set the top-level selector.
          //I grab the parents instead of doing a global
          //select because I don't want to conflict with
          //other tradestall elements on the form.
          var tradestalldiv = $(this).parentsUntil('.webform-component');

          //Grab the maximum number of stalls we can select
          //from the value of the hidden element.
          var maxvals = $(tradestalldiv.children('input:hidden')).val();

          //Grab the textfield accompaning this element.
          var textbox = tradestalldiv.find('input:text');
          //Grab the current value in the textfield.
          var currval = $(textbox).val();

          if($(this).hasClass("selected")) {
             var idnum = Drupal.tradestallRemovePrefix($(this).attr("id"));
             var newval = Drupal.tradestallRemoveItem(currval, idnum);
             textbox.val(newval);
             $(this).removeClass("selected");
             currstalls = currstalls - 1;
          }
          else if(currstalls < maxvals) {
            var id = $(this).attr("id");
            //If the textfield is empty add the clicked element's
            //id to the textbox. If it isn't empty then concatenate
            //the id to the contents of the textfield.
            var newval;
            if(currval == "") {
              newval = Drupal.tradestallRemovePrefix(id);
            }
            else {
              newval = currval + "," + Drupal.tradestallRemovePrefix(id);
            }
            $(textbox).val(newval);
            //Now change the class.
            $(this).addClass("selected");
            currstalls = currstalls + 1;
          }
        }
      });
    }
  };

})(jQuery);
